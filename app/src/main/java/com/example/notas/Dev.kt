package com.example.notas

import android.os.Bundle
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity

class Dev : AppCompatActivity() {
    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dev)
        val bundle = intent.extras
        val nombre = bundle?.get("INTENT_NOMBRE")
        val text1: TextView = findViewById(R.id.text1)
        text1.text = "$nombre"
        val code = bundle?.get("INTENT_CODIGO")
        val text2: TextView = findViewById(R.id.text2)
        text2.text = "Código: $code"
    }
}