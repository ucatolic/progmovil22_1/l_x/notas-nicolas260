package com.example.notas

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.supportActionBar?.hide()
        setContentView(R.layout.activity_main)

        val recyclerView = findViewById<RecyclerView>(R.id.recyclerView)
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = Scoreadapter()

        val btn: Button = findViewById(R.id.btn1)
        btn.setOnClickListener { dialog() }
    }
    fun dialog() {
        val intent = Intent(this, Dev::class.java)
        intent.putExtra("INTENT_NOMBRE", "Nicolas Javier Cardozo Diaz")
        intent.putExtra("INTENT_CODIGO", "67000260")
        startActivity(intent)
    }


}